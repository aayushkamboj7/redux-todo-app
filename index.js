let internalStore = {
  input: [],
};

let store = Redux.createStore(handleReducer);

function handleReducer(state = internalStore.input, action) {
  return (state = [...state, action.type]);
}

let toDoInput = document.querySelector('.toDoInput');

document.querySelector('.submit').addEventListener('click', () => {
  store.dispatch({ type: toDoInput.value });
});
let todos = document.querySelector('.todos');

store.subscribe(() => {
  toDoInput.value = '';
  store.getState().forEach((todo) => {
    let heading = document.createElement('h1');
    heading.innerText = todo;
    todos.append(heading);
  });
});
